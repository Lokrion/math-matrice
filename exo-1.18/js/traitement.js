var matriceA;

var nb_ligne;
var nb_column;


function saisirMatrice()
{
    nb_ligne = parseInt(document.getElementById('nb_ligne').value);
    nb_column = parseInt(document.getElementById('nb_column').value);
    
    saisirMatriceA();
    saisirMatriceB();
}


function saisirMatriceA()
{
    document.getElementById('saisi_matriceA').innerHTML = getTableauSaisiMatrice('matriceA');
}

function saisirMatriceB()
{

}


function getTableauSaisiMatrice(nom_matrice)
{
    var retour_html = '';
    for(var i=0; i < nb_ligne; i++) {
        retour_html += '<table>';
        for(var j=0; j < nb_column; j++) {
            retour_html += '<td>';
            retour_html += '<input type="numeric" required="required" id="'+ getIdCelluleMatrice(nom_matrice, i, j) + '" >';
            retour_html += '</td>';
        }
        retour_html += '</table>';
    }
    return retour_html;
}

function getIdCelluleMatrice(nom_matrice, num_ligne, num_colonne)
{
    return nom_matrice+'_'+num_ligne+'_'+num_colonne;
}

function saveMatriceA() {
    saveMatrice('matriceA', nb_ligne, nb_column);
}

function saveMatriceB() {
    saveMatrice('matriceB', nb_ligne, nb_column);
}

function saveMatrice(nom_matrice, num_ligne, num_colonne) {

    var matrice = [];
    for(var i=0; i < num_ligne; i++) {
        var ligne = [];
        for(var j=0; j < num_colonne; j++) {
            ligne.push(document.getElementById(getIdCelluleMatrice(nom_matrice, i, j)).value);
        }
        matrice.push(ligne);
    }

    $.each(matrice, function (i, c) {
        alert(i + "=>" + c)
    })
}

function getSommeMatrices(matrice1, matrice2) {
    if (matrice1 != null && matrice1.length > 0 && matrice1[0] != null && matrice1[0].length > 0
        && matrice2 != null && matrice2.length > 0 && matrice2[0] != null && matrice2[0].length > 0) {
        if (matrice1.length == matrice2.length && matrice1[0].length == matrice2[0].length) {
            var matriceSomme = [];
            for(var i=0; i < matrice1.length; i++) {
                var ligne = [];
                for(var j=0; j < matrice1[0].length; j++) {
                    ligne.push((matrice1[i][j] + matrice2[i][j]));
                }
                matriceSomme.push(ligne);
            }
            return matriceSomme
        } else {
            console.log("Les matrices ne sont pas de même dimension")
        }
    } else {
        console.log("Les matrices ne peuvent être NULL");
    }
}

function getProduitMatrice(matrice, coeficient) {
    if (matrice != null && matrice.length > 0 && matrice[0] != null && matrice[0].length > 0) {
        var matriceFinal = [];
        for(var i=0; i < nb_ligne; i++) {
            var ligne = [];
            for(var j=0; j < nb_column; j++) {
                ligne.push((matrice[i][j] * coeficient));
            }
            matriceFinal.push(ligne);
        }
        return matriceFinal;
    } else {
        console.log("La matrice ne peut être NULL");
    }
}

// todo finir le calcul
function getProduitMatrices(matrice1, matrice2) {
    if (matrice1 != null && matrice1.length > 0 && matrice1[0] != null && matrice1[0].length > 0
        && matrice2 != null && matrice2.length > 0 && matrice2[0] != null && matrice2[0].length > 0) {
        if (matrice1[0].length == matrice2.length) {
            var matriceFinal = [];
            for(var i=0; i < matrice1[0].length; i++) {
                var ligne = [];
                for(var j=0; j < matrice1[0].length; j++) {
                    ligne.push((matrice1[i][j] + matrice2[i][j]));
                }
                matriceFinal.push(ligne);
            }
            return matriceFinal
        } else {
            console.log("Les matrices ne sont pas de même dimention")
        }
    } else {
        console.log("Les matrices ne peuvent être NULL");
    }
}

function checkSquareMatrice(matrice) {
	var dim = matrice.length;
	for(var i=0;i<dim;i++) {
		if(matrice[i].length !== dim) {
			throw "Not a square matrice";
		}
	}
	return dim;
}

function getSubmatrice(matrice) {
	var dim = checkSquareMatrice(matrice);
	var overi = 0;
	var overj = 0;
	var res = [];
	for(var i = 0; i < dim; i++) {
		if(i === line) {
			overi=1;
			continue;
		}
		res[i-overi] = [];
		for(var j = 0; j < dim; j++) {
			if(j === column) {
				overj = 1;
				continue;
			}
			res[i - overi][j - overj] = matrice[i][j];
		}
	}
	return res; 
}

function getMatriceDeterminant(matrice) {
	var dim = checkSquareMatrice(matrice);
	if (dim === 2) {
		return (matrice[0][0]*matrice[1][1] - matrice[1][0]*matrice[0][1]);
	} else {
		var sum = 0;
		for(var i = 0; i < dim; i++) {
			sum += Math.pow(-1,i) * matrice[i][0] * getMatriceDeterminant(getSubmatrice(matrice,i,0));
		}
		return sum;
	}
}
